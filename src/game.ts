import {spawnGltfX, spawnEntity,spawnConeX,spawnCylinderX ,spawnBoxX, spawnPlaneX, spawnTextX} from './modules/SpawnerFunctions'
import {BuilderHUD} from './modules/BuilderHUD'
import utils from "../node_modules/decentraland-ecs-utils/index"
import { GroundCover } from './modules/GroundCover'



const scene = new Entity()
const transform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})

scene.addComponentOrReplace(transform)
engine.addEntity(scene)


const forestShape = new GLTFShape("models/treeset_wide.gltf")
const forest = spawnGltfX(forestShape, 7,0.2,8,  0,0,0, 0.55,0.55,0.55)

const margheritaShape = new GLTFShape("models/margheritas.glb")
const margherita0 = spawnGltfX(margheritaShape, 1,0,1, 0,0,0,  1,1,1)
const margherita1 = spawnGltfX(margheritaShape, 3,0,1, 0,0,0,  1,1,1)
const margherita2 = spawnGltfX(margheritaShape, 2,0,6, 0,0,0,  1,1,1)
const margherita3 = spawnGltfX(margheritaShape, 8,0,4, 0,0,0,  1,1,1)
const margherita4 = spawnGltfX(margheritaShape, 3,0,3, 0,0,0,  1,1,1)
const margherita5 = spawnGltfX(margheritaShape, 6,0,9, 0,0,0,  1,1,1)
const margherita6 = spawnGltfX(margheritaShape, 8,0,7, 0,0,0,  1,1,1)
const margherita7 = spawnGltfX(margheritaShape, 11,0,4, 0,0,0,  1,1,1)
const margherita8 = spawnGltfX(margheritaShape, 12,0,6, 0,0,0,  1,1,1)
const margherita9 = spawnGltfX(margheritaShape, 3,0,14, 0,0,0,  1,1,1)
const margherita10 = spawnGltfX(margheritaShape, 15,0,3, 0,0,0,  1,1,1)
const margherita11 = spawnGltfX(margheritaShape, 8,0,4, 0,0,0,  1,1,1)
const margherita12 = spawnGltfX(margheritaShape, 9,0,11, 0,0,0,  1,1,1)
const margherita13 = spawnGltfX(margheritaShape, 1,0,12, 0,0,0,  1,1,1)
const margherita14 = spawnGltfX(margheritaShape, 15,0,7, 0,0,0,  1,1,1)
const margherita15 = spawnGltfX(margheritaShape, 1,0,3, 0,0,0,  1,1,1)
const margherita16 = spawnGltfX(margheritaShape, 13,0,9, 0,0,0,  1,1,1)
const margherita17 = spawnGltfX(margheritaShape, 1,0,14, 0,0,0,  1,1,1)
const margherita18 = spawnGltfX(margheritaShape, 7,0,8, 0,0,0,  1,1,1)
const margherita19 = spawnGltfX(margheritaShape, 6,0,4, 0,0,0,  1,1,1)
const margherita20 = spawnGltfX(margheritaShape, 12,0,11, 0,0,0,  1,1,1)
const margherita21 = spawnGltfX(margheritaShape, 5,0,13, 0,0,0,  1,1,1)
const margherita22 = spawnGltfX(margheritaShape, 10,0,8, 0,0,0,  1,1,1)
const margherita23 = spawnGltfX(margheritaShape, 7,0,13, 0,0,0,  1,1,1)
const margherita24 = spawnGltfX(margheritaShape, 4,0,5, 0,0,0,  1,1,1)
const margherita25 = spawnGltfX(margheritaShape, 8,0,2, 0,0,0,  1,1,1)
const margherita26 = spawnGltfX(margheritaShape, 1,0,9, 0,0,0,  1,1,1)
const margherita27 = spawnGltfX(margheritaShape, 3,0,9, 0,0,0,  1,1,1)
const margherita28 = spawnGltfX(margheritaShape, 5,0,4, 0,0,0,  1,1,1)
const margherita29 = spawnGltfX(margheritaShape, 8,0,12, 0,0,0,  1,1,1)
const margherita30 = spawnGltfX(margheritaShape, 6.5,0,5.5, 0,0,0,  1,1,1)



const grassyFineTexture = new Texture("materials/Tileable-Textures/grassy-512-1-0.png")
const grassyFineMaterial = new Material()
grassyFineMaterial.albedoTexture = grassyFineTexture
const ground = new GroundCover (0,0,16,0.02,16,grassyFineMaterial,false)
ground.setParent(scene)


const animator = new Animator();
let clipTreeButt1 = new AnimationState("Armature_Idle")
//clipWalk.looping = true
clipTreeButt1.setParams({speed: 1, weight: 1})
animator.addClip(clipTreeButt1);
forest.addComponent(animator);
clipTreeButt1.play();


const butterflyShape = new GLTFShape("models/mybutt.gltf")
const freeButterfly0 = spawnGltfX(butterflyShape, 2.8,-6.6,3.1,  -180,0,-180,  3,3,3)

const freeButterflyAnimator = new Animator();
let clipFreeButterfly0 = new AnimationState("Armature_Idle_Butterfly.000")
clipFreeButterfly0.setParams({speed: 5, weight: 1})
freeButterflyAnimator.addClip(clipFreeButterfly0);
freeButterfly0.addComponent(freeButterflyAnimator);
clipFreeButterfly0.play();

//"Armature_Idle_Butterfly.000"


const point1 = new Vector3(2.8,-6.6,3.1)
//const point2 = new Vector3(Math.random() , Math.random(), Math.random())
const point2 = new Vector3(7.8,-6.6,7.1)
const point3 = new Vector3(5.8,-6.6,9.1)
const point4 = new Vector3(1.8, -6.6, 6.1)
const path: Vector3[] = [point1, point2, point3, point4]

const TURN_TIME = 0.9

@Component('lerpData')
export class LerpData {
  array: Vector3[] = path
  origin: number = 0
  target: number = 1
  fraction: number = 0
}

@Component("timeOut")
export class TimeOut {
  timeLeft: number
  constructor( time: number){
    this.timeLeft = time
  }
}

export const paused = engine.getComponentGroup(TimeOut)


export class ButterflyFly {
  update(dt: number) {
    let transform = freeButterfly0.getComponent(Transform)
    let path = freeButterfly0.getComponent(LerpData)
    path.fraction += dt / 1
    if (path.fraction < 1) {
      transform.position = Vector3.Lerp(
        path.array[path.origin],
        path.array[path.target],
        path.fraction
      )
    } else {
      path.origin = path.target
      path.target += 1
      if (path.target >= path.array.length) {
        path.target = 0
      }
      path.fraction = 0
      transform.lookAt(path.array[path.target])
    }
  }
}

engine.addSystem(new ButterflyFly)
freeButterfly0.addComponent(new LerpData())

const hud:BuilderHUD =  new BuilderHUD()
hud.setDefaultParent(scene)
hud.attachToEntity(freeButterfly0)